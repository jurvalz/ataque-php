<?php

define('UD_URL','http://inversionesvizac.com/vizac2/api/productos/actualizar.php');

$proyecto = 93;

function data($proyecto,$id){
    $rand = 1;
    $val = array(
        "idProducto"=> "$id",
        "CodInterno"=> "",
        "idInventario"=> "$proyecto",
        "CodOtros"=> "",
        "Condicion"=> "INVENTARIADO",
        "CodActual"=> "1",
        "CodInvAnterior"=> "1",
        "CodInvAnteriorAnterior"=> "1",
        "CodSbn"=> "X",
        "idDenominacion"=> "1",
        "idDenominacionInv"=> "1",
        "Estado"=> "X",
        "Situacion"=> "1",
        "Marca"=> "1",
        "Modelo"=> "1",
        "TipoModelo"=> "1",
        "idColor"=> "1",
        "Serie"=> "1",
        "Medidas"=> "1",
        "Observacion"=> "1",
        "Clase"=> "1",
        "Carroceria"=> "1",
        "Placa"=> "1",
        "AnioFabricacion"=> "1",
        "NroMotor"=> "1",
        "NroChasis"=> "1",
        "idEncargado"=> "1",
        "id_local"=> "1",
        "idDependencia"=> "1",
        "Oficina"=> "1",
        "Area"=> "1",
        "Piso"=> "1",
        "idEncargadoAnt"=> "1",
        "id_local_ant"=> "1",
        "idDependencia_ant"=> "1",
        "Oficina_ant"=> "",
        "Area_ant"=> "1",
        "Piso_ant"=> "1",
        "Observacion_ant"=> "1",
        "Img"=> "1",
        "Img2"=> "1",
        "Img3"=> "1",
        "flgActivo"=> "1",
        "Tipo_Inv"=> "1",
        "idUsuarioReg"=> "1",
        "FechaReg"=> "1",
        "idUsuarioInv"=> "2",
        "FechaInv"=> "1",
        "NroFicha"=> "1"
    );
    $json = json_encode($val);
    return $json;
}

for($i=1;$i<1000000;$i++){
    $id = $i;
    $curl= curl_init(UD_URL);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_POSTFIELDS, data($proyecto,$id));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Accept: */*",
        'Content-Type: application/json',
        'Content-Length: ' . strlen(data($proyecto,$id))
    ));
    $response = curl_exec($curl);
    curl_close($curl);
    print($i."=> ");
    print_r($response."\n");
}